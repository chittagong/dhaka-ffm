resource "kubernetes_deployment_v1" "user" {
  metadata {
    name = "user"
    labels = {
      test = "user"
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        test = "user"
      }
    }

    template {
      metadata {
        labels = {
          test = "user"
        }
      }

      spec {
        container {
          image = "385211315249.dkr.ecr.us-east-1.amazonaws.com/dhaka-user-msg:latest"
          name  = "user"

          resources {
            limits = {
              cpu    = "1"
              memory = "1G"
            }
            requests = {
              cpu    = ".5m"
              memory = "512Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service_v1" "user_svc" {
  metadata {
    name = "user"
  }
  spec {
    selector = {
      test = kubernetes_deployment_v1.user.metadata.0.labels.test
    }
    
    port {
      port        = 3000
      target_port = 3000
    }

    type = "NodePort"
  }
}
